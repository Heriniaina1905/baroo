package beans.bases;

import java.sql.Date;

public abstract class Personne {
    private String nom;
    private String sexe;
    private Date dn;
    private Personne[] amis;
    // private Personne[] parents;
    // private Personne[] enfants;

    public String getNom(){
        return this.nom;
    }
    public void setNom(String nm){
        this.nom = nm;
    }

    public String getSexe(){
        return this.sexe;
    }
    public void setSexe(String sx){
        this.sexe = sx;
    }

    public Date getDn(){
        return this.dn;
    }
    public void setDn(Date nm){
        this.dn = nm;
    }
    
    public Personne[] getAmis(){
        return this.amis;
    }
    public void setAmis(Personne[] am){
        this.amis = am;
    }

    public Personne getOneAmi(int i){
        return this.amis[i];
    }

    public int getIndiceLastAmi(){
        int i = 0;
        while(this.amis[i] != null){
            i+=1;
        }
        return i;
    }

    public void addAmis(Personne p){
        int i = getIndiceLastAmi();
        this.amis[i+1] = p;
    }

    // public Personne[] getParents(){
    //     return this.parents;
    // }
    // public void setParents(Personne[] p){
    //     this.parents =p;
    // }

    // public Personne[] getEnfants(){
    //     return this.enfants;
    // }
    // public void setEnfants(Personne[] p){
    //     this.enfants =p;
    // }

    // public Personne getParents(int i){
    //     return this.parents[i];
    // }

    // public Personne getEnfants(int i){
    //     return this.enfants[i];
    // }
}